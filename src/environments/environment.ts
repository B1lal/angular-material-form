// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBiNOSZYKfuJ0vM0x3MfrieIx1ABqVw46k',
    authDomain: 'form-registration-37b93.firebaseapp.com',
    databaseURL: 'https://form-registration-37b93.firebaseio.com',
    projectId: 'form-registration-37b93',
    storageBucket: 'form-registration-37b93.appspot.com',
    messagingSenderId: '460163719630',
    appId: '1:460163719630:web:9a632bd4421fe535634b9f',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
