import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegistrationComponent } from './registration/registration.component';
import { UserFormComponent } from './registration/user-form/user-form.component';

// import the material module
import { MaterialModule } from './material/material.module';

// import user form service
import { UserFormService } from './shared/user-form.service';

// import reactive forms
import { ReactiveFormsModule } from '@angular/forms';

// Imports related to firebase database
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from '../environments/environment';
import { UserListComponent } from './registration/user-list/user-list.component';

@NgModule({
  declarations: [
    AppComponent,
    RegistrationComponent,
    UserFormComponent,
    UserListComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    RouterModule.forRoot([
      { path: '', component: UserFormComponent },
      { path: 'addUser', component: UserFormComponent },
      { path: 'listUsers', component: UserListComponent },
    ]),
  ],
  providers: [UserFormService],
  bootstrap: [AppComponent],
})
export class AppModule {}
