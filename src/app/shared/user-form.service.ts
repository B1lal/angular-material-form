import { Injectable } from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
} from '@angular/forms';

import { matchEmail, malaysianNumber } from './user-from-validators';

// Imports related to firebase database
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

@Injectable({
  providedIn: 'root',
})
export class UserFormService {
  form: FormGroup;

  constructor(private fb: FormBuilder, private firebase: AngularFireDatabase) {
    this.form = this.fb.group({
      $key: new FormControl(null),
      title: new FormControl('', Validators.required),
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.email, Validators.required]),
      confirmEmail: new FormControl('', [Validators.required, matchEmail]),
      dob: new FormControl('', [Validators.required]),
      phoneNumber: new FormControl('', [
        Validators.required,
        malaysianNumber,
        Validators.maxLength(11),
      ]),
    });

    this.form.controls.email.valueChanges.subscribe((x) =>
      this.form.controls.confirmEmail.updateValueAndValidity()
    );
  }

  initializeUserFormGroup() {
    this.form.setValue({
      $key: null,
      title: '',
      firstName: '',
      lastName: '',
      email: '',
      confirmEmail: '',
      dob: '',
      phoneNumber: '',
    });
  }

  userList: AngularFireList<any>;

  // Method to retrieve all users from the database
  getUsers() {
    this.userList = this.firebase.list('users');
    return this.userList.snapshotChanges();
  }

  // Method to insert a user to the database
  insertUser(user) {
    // Reformating the date which will be uploaded to firebase
    let day = user.dob.getDate();
    let month = user.dob.getMonth() + 1;
    const year = user.dob.getFullYear();

    let formatedDate = `${day}/${month}/${year}`;

    this.userList.push({
      title: user.title,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      dob: formatedDate,
      phoneNumber: user.phoneNumber,
    });
  }

  // Method to update a user in the database
  updateUser(user) {
    let day = user.dob.getDate();
    let month = user.dob.getMonth() + 1;
    const year = user.dob.getFullYear();

    let formatedDate = `${day}/${month}/${year}`;

    this.userList.update(user.$key, {
      title: user.title,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      dob: formatedDate,
      phoneNumber: user.phoneNumber,
    });
  }

  deleteEmployee($key: string) {
    this.userList.remove($key);
  }
}
