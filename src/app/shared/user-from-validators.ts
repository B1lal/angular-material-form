import { AbstractControl } from '@angular/forms';

export function matchEmail(control: AbstractControl) {
  if (control && (control.value !== null || control.value !== undefined)) {
    const confirmEmailVal = control.value;

    const emailControl = control.root.get('email');

    if (emailControl) {
      const emailVal = emailControl.value;
      if (emailVal !== confirmEmailVal) {
        return {
          isError: true,
        };
      }
    }
  }
  return null;
}

export function malaysianNumber(control: AbstractControl) {
  if (control && (control.value !== null || control.value !== undefined)) {
    const phoneRegex = new RegExp('^(01)[0-46-9]*[0-9]{7,8}$');

    if (!phoneRegex.test(control.value)) {
      return {
        isError: true,
      };
    }
  }
  return null;
}
