import { Component, OnInit } from '@angular/core';
import { UserFormService } from 'src/app/shared/user-form.service';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
})
export class UserListComponent implements OnInit {
  constructor(private service: UserFormService) {}

  listData: MatTableDataSource<any>;
  displayedColumns: string[] = [
    'title',
    'firstName',
    'lastName',
    'email',
    'dob',
    'phoneNumber',
  ];

  ngOnInit(): void {
    this.service.getUsers().subscribe((list) => {
      let array = list.map((item) => {
        return {
          $key: item.key,
          ...item.payload.val(),
        };
      });
      this.listData = new MatTableDataSource(array);
    });
  }
}
