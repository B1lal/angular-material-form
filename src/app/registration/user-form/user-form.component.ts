import { Component, OnInit } from '@angular/core';

// import user form service
import { UserFormService } from '../../shared/user-form.service';
// import the notification service
import { NotificationService } from '../../shared/notification.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css'],
})
export class UserFormComponent implements OnInit {
  constructor(
    public service: UserFormService,
    public notification: NotificationService
  ) {}

  titles = [
    { id: 1, value: 'Mr' },
    { id: 2, value: 'Mrs' },
    { id: 3, value: 'Prof' },
  ];

  ngOnInit(): void {
    this.service.getUsers();
  }

  onSubmit() {
    if (this.service.form.valid) {
      this.service.insertUser(this.service.form.value);
      this.service.form.reset();
      this.service.initializeUserFormGroup();
      this.notification.success('User Data Added');
    }
  }
}
